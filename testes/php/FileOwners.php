<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
    public static function groupByOwners($files){
    	$nomeAutor = $files[0][0];
        $lista = array($nomeAutor => array());

        for ($i=0; $i < count($files) ; $i++) { 
        	foreach ($lista as $value) {
        		if ($value != $files[$i][0]) {
                    $nomeAutor = $files[$i][0];
                    $lista[$nomeAutor] = array();
                    break;
                }
                else{
                   $lista[$valor] = $files[$i];
                   break;
                }
        	}
        }
        return $lista;
    }
}

$files = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria",

);
var_dump(FileOwners::groupByOwners($files));