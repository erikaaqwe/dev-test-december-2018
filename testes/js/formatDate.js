// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.

// format from M/D/YYYY to YYYYMMDD
function formatDate(userDate) {
	let string = userDate.replace(/[\/]/g , "");
	let newstring= string[4]+string[5]+string[6]+string[7]+string[0]+string[1]+string[2]+string[3];
	return newstring;
}

console.log(formatDate("12/31/2014"));